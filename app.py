from flask import Flask, jsonify, request, redirect, url_for
from flask_cors import CORS
from sqlalchemy import create_engine
from sqlalchemy.sql import text
import json
import secrets 


app = Flask(__name__)
CORS(app)
app.config['JSON_SORT_KEYS'] = False

conn_str = 'postgresql://postgres:Omron_306043@localhost:5432/coffee3'
engine = create_engine(conn_str, echo=False)

@app.route('/index', methods=['GET'])
def greetings():
    return "Hello"

@app.route('/user', methods=['GET'])
def get_users():
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.user ORDER BY user_id")
            result = connection.execute(qry)
            for item in result:
                all.append({
                    'user_id':item[0], 'name':item[1], 'email':item[2], 'phone':item[3], 'address':item[4], 'password':item[5]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/user/<user_id>', methods=['GET'])
def get_user_by_id(user_id):
    user_id = user_id
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.user WHERE public.user.user_id=:user_id")
            result = connection.execute(qry, user_id=user_id)
            for item in result:
                all.append({
                    'user_id':item[0], 'name':item[1], 'email':item[2], 'phone':item[3], 'address':item[4], 'password':item[5]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/user/add', methods=['POST'])
def add_user():
    body_req = request.json
    name = body_req.get('name')
    email = body_req.get('email')
    phone = body_req.get('phone')
    address = body_req.get('address')
    password = body_req.get('password')
    try:
        with engine.connect() as connection:
            qry = text("INSERT INTO public.user (name, email, phone, address, password)\
                        VALUES (:name, :email, :phone, :address, :password)")
            connection.execute(qry, name=name, email=email, phone=phone, address=address, password=password)
            new_qry = text("SELECT * FROM public.user WHERE name=:name")
            new_result = connection.execute(new_qry, name=name)
            all = []
            for item in new_result:
                all.append({
                    'user_id':item[0], 'name':item[1], 'email':item[2], 'phone':item[3], 'address':item[4], 'password':item[5]
                    })
            # return redirect(url_for)
            return jsonify(status='success create account', data=all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/user/update/<_id>', methods=['PUT'])
def update_user(_id):
    body_req = request.json
    user_id = _id
    name = body_req.get('name')
    email = body_req.get('email')
    phone = body_req.get('phone')
    address = body_req.get('address')
    password = body_req.get('password')
    try:
        with engine.connect() as connection:
            qry = text("UPDATE public.user SET name=:name, email=:email, phone=:phone, address=:address, password=:password \
                        WHERE user_id=:user_id")
            connection.execute(qry, name=name, email=email, phone=phone, address=address, password=password, user_id=user_id)
            new_qry = text(" SELECT * FROM public.user WHERE name=:name ")
            new_result = connection.execute(new_qry, name=name)
            all = []
            for item in new_result:
                all.append({
                'user_id':item[0], 'name':item[1], 'email':item[2], 'phone':item[3], 'address':item[4], 'password':item[5]
                })
            return jsonify(status='success update',data=all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/process-order/<user_id>', methods=['GET'])
def user_process_order(user_id):
    user_id = user_id
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT public.order.order_id, public.order.order_date, public.order.status, \
                        SUM(public.bucket.price) as total FROM public.bucket JOIN public.order USING(order_id) JOIN public.user \
                        USING(user_id) WHERE public.user.user_id=:user_id and public.order.status='process'\
                        GROUP BY public.order.order_id, public.order.order_id, public.user.name ORDER BY order_id")
            result = connection.execute(qry, user_id=user_id)
            for item in result:
                all.append({
                    "order_id":item[0], "order_date":item[1], "order_status":item[2], "total":item[3]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/finish-order/<user_id>', methods=['GET'])
def user_finish_order(user_id):
    user_id = user_id
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT public.order.order_id, public.order.order_date, public.order.status, \
                        SUM(public.bucket.price) as total FROM public.bucket JOIN public.order USING(order_id) JOIN public.user \
                        USING(user_id) WHERE public.user.user_id=:user_id and public.order.status!='process' \
                        GROUP BY public.order.order_id, public.order.order_id, public.user.name \
                        ORDER BY order_id")
            result = connection.execute(qry, user_id=user_id)
            for item in result:
                all.append({
                    "order_id":item[0], "order_date":item[1], "order_status":item[2], "total":item[3]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))


# LOGIN USER
@app.route('/login', methods=['POST'])
def login_user():
    body = request.json
    user_name = body.get('user_name')
    password = body.get('password')
    try:
        with engine.connect() as connection:
            qry1=text("select public.user.user_id, public.user.name, \
                        public.user.password from public.user where \
                        public.user.name=:name AND public.user. \
                        password=:password")
            result1=connection.execute(qry1, name=user_name, password=password)
            info = []
            for value in result1:
                info.append({
                    "login_id": value['user_id'],
                    "login_pass": value['password']
                })
            if info == []:
                return jsonify(message="wrong pass or username"), 401
            else:
                login_id = info[0]['login_id']
                # login_pass = info[0]['login_pass']
                gen_token = secrets.token_hex()
                qry2 = text("INSERT INTO public.token(user_token, user_id) VALUES (:user_token, :user_id)")
                connection.execute(qry2, user_token=gen_token, user_id=login_id)
                # qry3 = text("SELECT * FROM public.token WHERE user_token=:user_token")
                qry3 = text("SELECT public.token.user_id, public.user.name, public.token.user_token FROM public.token \
                             JOIN public.user USING(user_id) WHERE public.token.user_token=:user_token")
                result3 = connection.execute(qry3, user_token=gen_token)
                result4 = []
                for item in result3:
                    result4.append({
                        "user_id" : item["user_id"],
                        "name" : item["name"],
                        "user_token" : item["user_token"]
                    })
                return jsonify(result4), 200
    except Exception as e:
        return jsonify(error=str(e))

# LOGOUT USER
@app.route('/logout', methods = ['DELETE'])
def logout():
    body = request.json
    user_token = body.get('user_token')
    try:
        with engine.connect() as connection:
            query = text("DELETE FROM public.token WHERE \
                        public.token.user_token=:user_token")
            connection.execute(query, user_token = user_token)
            return jsonify(token = user_token, status = 'logout')
    except Exception as e:
        return jsonify(error=str(e))

# ALL TOKEN
@app.route('/token/all', methods = ['GET'])
def get_all_tokens():
    all = []
    try:
        with engine.connect() as connection:
            query = text("SELECT * FROM public.token ORDER BY public.token.user_id")
            result = connection.execute(query)
            for row in result:
                all.append({
                    'user_id' : row['user_id'], 
                    'token' : row['user_token']
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu', methods=['GET'])
def get_menus():
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.menu")
            result = connection.execute(qry)
            for item in result:
                all.append({
                    'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]
                })
            return(jsonify(all))
    except Exception as e:
        return jsonify(error=str(e))

# GET MENU BY MENU_ID to FETCH IN UPDATE MENU and ORDER MENU ACTIVITY
@app.route('/menu/<menu_id>', methods=['GET'])
def get_menu_by_id(menu_id):
    menu_id = menu_id
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.menu WHERE menu_id=:menu_id")
            result = connection.execute(qry, menu_id=menu_id)
            for item in result:
                all.append({
                    'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]
                })
                # print(all[0]['menu'])
            return(jsonify(all))
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu/add', methods=['POST'])
def add_menu():
    body_req = request.json
    menu_id = body_req.get('menu_id')
    menu = body_req.get('menu')
    description = body_req.get('description')
    price = body_req.get('price')
    stock = body_req.get('stock')
    try:
        with engine.connect() as connection:
            qry = text("INSERT INTO public.menu (menu_id, menu, description, price, stock) \
                        VALUES (:menu_id, :menu, :description, :price, :stock)")
            connection.execute(qry, menu_id=menu_id, menu=menu, description=description, price=price, stock=stock)
            new_qry = text("SELECT * FROM public.menu WHERE menu_id=:menu_id")
            result = connection.execute(new_qry, menu_id=menu_id)
            new_menu = []
            for item in result:
                new_menu.append({
                'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]        
                })
            return jsonify(status="success add new menu", new_menu=new_menu)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu/update/<menu_id>', methods=['PUT'])
def update_menu(menu_id):
    body_req = request.json
    menu_id = menu_id
    menu = body_req.get('menu')
    description = body_req.get('description')
    price = body_req.get('price')
    try:
        with engine.connect() as connection:
            qry = text("UPDATE public.menu SET menu=:menu, description=:description, price=:price \
                        WHERE menu_id=:menu_id ")
            connection.execute(qry, menu_id=menu_id, menu=menu, description=description, price=price)
            new_qry = text("SELECT * FROM public.menu WHERE menu_id=:menu_id")
            new_result = connection.execute(new_qry, menu_id=menu_id)
            all = []
            for item in new_result:
                all.append({
                'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]        
                })
            return jsonify(status='success update',data=all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu/update-stock', methods=['PUT'])
def update_stock():
    body_req = request.json
    menu_id = body_req.get('menu_id')
    stock = body_req.get('stock')
    try:
        with engine.connect() as connection:
            qry = text("UPDATE public.menu SET stock=(SELECT stock FROM menu \
                        WHERE menu_id=:menu_id)+:stock WHERE menu_id=:menu_id")
            connection.execute(qry, stock=stock, menu_id=menu_id)
            new_qry = text("SELECT * FROM public.menu WHERE menu_id=:menu_id")
            result = connection.execute(new_qry, menu_id=menu_id)
            new_stock = []
            for item in result:
                new_stock.append({
                    'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]
                })
            return jsonify(status='success update stock', data=new_stock)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/order', methods=['GET'])
def get_orders():
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.order ORDER BY order_id")
            result = connection.execute(qry)
            all = []
            for item in result:
                all.append({
                    'order_id':item[0], 'user_id':item[1], 'status':item[2], 'order_date':item[3]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

# @app.route('/add-order', methods=['POST'])
# def add_order():
    body_req = request.json
    user_id = body_req.get('user_id')
    try:
        with engine.connect() as connection:
            qry1 = text("SELECT count(status) as status FROM public.order \
                        WHERE status ='process' AND user_id=:user_id GROUP BY status")
            result = connection.execute(qry1, user_id=user_id)
            active = [item for item in result]

            if active == [] or active[0][0] < 10:
                # print(active)
                qry2 = text("INSERT INTO public.order (user_id, status) VALUES (:user_id, :status)")
                connection.execute(qry2, user_id=user_id, status='process')
                qry3 = text("SELECT * FROM public.order ORDER BY order_id DESC LIMIT 1")
                result3 = connection.execute(qry3)
                new_order = []
                for item in result3:
                    new_order.append({
                        'order_id':item[0], 'user_id':item[1], 'status':item[2], 'order_date':item[3]
                    })
                return jsonify(data=new_order)
            else:
                return jsonify(status='Maximum Order')
    except Exception as e:
        return jsonify(error=str(e))

# @app.route('/order-detail', methods=['POST'])
# def order_detail():
    body_req = request.json
    order_id = body_req.get('order_id')
    menu_id = body_req.get('menu_id')
    quantity = body_req.get('quantity')
    try:
        with engine.connect() as connection:
            # Enter the detail with price 0
            for index, item in enumerate(menu_id):
                qry = text("INSERT INTO public.bucket (order_id, menu_id, quantity, price) \
                            VALUES (:order_id, :menu_id, :quantity, :price)")
                connection.execute(qry, order_id=order_id, menu_id=item, quantity=quantity[index], price=0)
            # Calculate the price depend on quantity
            for item in range(len(menu_id)):
                qry = text("UPDATE public.bucket SET price=(SELECT price FROM public.menu \
                            WHERE menu_id=:menu_id)*quantity WHERE order_id=:order_id AND menu_id=:menu_id")
                connection.execute(qry, order_id=order_id, menu_id=menu_id[item])
            # Update/substrack stock in menu table depend on quantity
            for item in range(len(menu_id)):
                qry = text("UPDATE public.menu SET stock=stock-(SELECT quantity FROM public.bucket \
                            WHERE order_id=:order_id AND menu_id=:menu_id) WHERE menu_id=:menu_id")
                connection.execute(qry, order_id=order_id, menu_id=menu_id[item])
            qry = text("SELECT public.order.order_id, public.order.user_id, public.user.name, SUM(public.bucket.price) FROM public.bucket \
                        JOIN public.order USING(order_id) JOIN public.user USING(user_id) \
                        WHERE public.bucket.order_id=:order_id GROUP BY public.order.order_id, public.user.name \
                        ORDER BY order_id")
            result = connection.execute(qry, order_id=order_id)
            detail = []
            for item in result:
                detail.append({
                    'order_id':item[0], 'user_id':item[1], 'name':item[2], 'total':item[3]
                })
            return jsonify(detail)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/user-order', methods=['POST'])
def user_order():
    body_req = request.json
    user_id = body_req.get('user_id')
    menu_id = body_req.get('menu_id')
    quantity = body_req.get('quantity')
    # order_id = body_req.get('order_id) ==> dapat dari line 416
    try:
        with engine.connect() as connection:
            qry1 = text("SELECT count(status) as status FROM public.order WHERE status ='process' AND user_id=:user_id GROUP BY status")
            result = connection.execute(qry1, user_id=user_id)
            active = [item for item in result]

            if active == [] or active[0][0] < 10:
                qry2 = text("INSERT INTO public.order (user_id, status) VALUES (:user_id, :status)")
                connection.execute(qry2, user_id=user_id, status='process')
                qry3 = text("SELECT public.order.order_id FROM public.order WHERE public.order.user_id=:user_id \
                            ORDER BY order_date DESC LIMIT 1")
                result3 = connection.execute(qry3, user_id=user_id)
                get_order_id = [item for item in result3]
                # print(get_order_id) # print(get_order_id[0]) # print(get_order_id[0][0])

                order_id = get_order_id[0][0]
                # masukkan detail dengan harga 0
                for index, item in enumerate(menu_id):
                    qry = text("INSERT INTO public.bucket (order_id, menu_id, quantity, price) \
                                VALUES (:order_id, :menu_id, :quantity, :price)")
                    connection.execute(qry, order_id=order_id, menu_id=item, quantity=quantity[index], price=0)
                # hitung harga berdasarkan jumlah pembelian
                for item in range(len(menu_id)):
                    qry = text("UPDATE public.bucket SET price=(SELECT price FROM public.menu \
                                WHERE menu_id=:menu_id)*quantity WHERE order_id=:order_id AND menu_id=:menu_id")
                    connection.execute(qry, order_id=order_id, menu_id=menu_id[item])
                # kurangi stok menu di tabel menu berdasarkan jumlah pembelian
                for item in range(len(menu_id)):
                    qry = text("UPDATE public.menu SET stock=stock-(SELECT quantity FROM public.bucket \
                                WHERE order_id=:order_id AND menu_id=:menu_id) WHERE menu_id=:menu_id")
                    connection.execute(qry, order_id=order_id, menu_id=menu_id[item])
                qry = text("SELECT public.order.order_id, public.order.user_id, public.user.name, SUM(public.bucket.price) FROM public.bucket \
                            JOIN public.order USING(order_id) JOIN public.user USING(user_id) \
                            WHERE public.bucket.order_id=:order_id GROUP BY public.order.order_id, public.user.name \
                            ORDER BY order_id")
                result = connection.execute(qry, order_id=order_id)
                detail = []
                for item in result:
                    detail.append({
                        'order_id':item[0], 'user_id':item[1], 'name':item[2], 'total':item[3]
                    })
                return jsonify(detail)
            else:
                return jsonify(status='Maximum Order'), 401
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/check-session', methods=['POST'])
def order_coffee():
    body_req = request.json
    user_token = body_req.get('user_token') # ambil dari session user
    try:
        with engine.connect() as connection:
            # ambil token masing2 user
            qry1 = text("SELECT * FROM public.token WHERE user_token=:user_token")
            result1 = connection.execute(qry1, user_token=user_token)
            all = []
            for item in result1:
                all.append({
                    "user_id":item[1], "user_token":item[0]
                })
            if all != []:
                return jsonify(all), 200
            else:
                return jsonify(message="kakak belum buat akun atau masuk"), 401
    except Exception as e:
        return jsonify(error=str(e))

    # return jsonify(hasil=(
    #     {'data':'masuk', 'status':200}
    #     ))
    # return ({
    #     'data' : 'masuk',
    #     'status' : 200
    # })

@app.route('/order/complete/<order_id>', methods=['PUT'])
def update_to_complete(order_id):
    order_id = order_id
    try:
        with engine.connect() as connection:
            qry1 = text("UPDATE public.order SET status='complete' WHERE order_id=:order_id")
            connection.execute(qry1, order_id=order_id)
            qry2 = text("SELECT * FROM public.order WHERE order_id=:order_id")
            result = connection.execute(qry2, order_id=order_id)
            status = []
            for item in result:
                status.append({
                    'order_id':item[0], 'user_id':item[1], 'status':item[2]
                })
            return jsonify(status)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/order/cancel/<order_id>', methods=['PUT'])
def update_to_cancel(order_id):
    order_id = order_id
    try:
        with engine.connect() as connection:
            qry1 = text("UPDATE public.order SET status='cancel' WHERE order_id=:order_id")
            connection.execute(qry1, order_id=order_id)
            qry2 = text("SELECT * FROM public.order WHERE order_id=:order_id")
            result = connection.execute(qry2, order_id=order_id)
            status = []
            for item in result:
                status.append({
                    'order_id':item[0], 'user_id':item[1], 'status':item[2]
                })
            return jsonify(status)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/bucket', methods=['GET'])
def get_bucket():
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.bucket \
                        GROUP BY order_id, menu_id \
                        ORDER BY order_id")
            result = connection.execute(qry)
            all = []
            for item in result:
                all.append({
                    'order_id':item[0], 'menu_id':item[1], 'quantity':item[2], 'price':item[3]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/bucket/<id>', methods=['GET'])
def get_bucket_using_id(id):
    id = id
    try:
        with engine.connect() as connection:
            qry = text("SELECT public.bucket.order_id, public.menu.menu, public.bucket.quantity, \
                        public.menu.price as price_per_item, public.bucket.price as total FROM public.bucket \
                        JOIN public.menu USING(menu_id) WHERE order_id=:order_id")
            result = connection.execute(qry, order_id=id) 
            all = []
            for item in result:
                all.append({
                    'menu_id':item[1], 'quantity':item[2], 'price':item[3], 'total':item[4]
                })
            order_id = item[0]
            return jsonify(order_id=order_id, data=all)
            # return jsonify(all)

            # menu, qty, price, total = [], [], [], []
            # for item in result:
            #     menu.append(item[1])
            #     qty.append(item[2])
            #     price.append(item[3])
            #     total.append(item[4])
            # return jsonify({'order_id':item[0], 'menu':menu, 'quantity':qty, 'price':price, 'total':total})
    except Exception as e:
        return jsonify(error=str(e))

# DETAIL STATUS TRANSACTION PER USER 
@app.route('/user/status/<user_id>', methods=['GET'])
def get_status_user(user_id):
    user_id = user_id
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT public.user.user_id, public.user.name, public.order.order_id, public.menu.menu_id, \
                        public.menu.price, public.bucket.quantity, public.order.status FROM public.bucket \
	                    JOIN public.order USING(order_id) JOIN public.user USING(user_id) JOIN public.menu USING(menu_id) \
                        WHERE public.user.user_id=:user_id ORDER BY order_id")
            result = connection.execute(qry, user_id=user_id)
            for item in result:
                # print(item)
                all.append({
                    'user_id':item[0], 'name':item[1], 'order_id':item[2], 'menu_id':item[3], 'price':item[4], 'quantity':item[5], 'status':item[6]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/check-active', methods=['GET'])
def check_active():
    try:
        with engine.connect() as connection:
            qry = text("SELECT user_id, name, count(status) as service FROM public.order \
                        JOIN public.user USING(user_id) WHERE status='process' GROUP BY user_id, name ORDER BY user_id")
            result = connection.execute(qry)
            process = []
            for item in result:
                process.append({
                    'user_id':item[0], 'name':item[1], 'service':item[2]
                })
            if process == []:
                return jsonify(process=0)
            else:
                return jsonify(process)
    except Exception as e:
        return jsonify(error=str(e))

# SHOW MENU FOR USER NO NEED LOGIN
@app.route('/menu-stock', methods=['GET'])
def get_menu_in_stock():
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.menu WHERE stock > 0")
            result = connection.execute(qry)
            for item in result:
                all.append({
                    'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]
                })
            return(jsonify(all))
    except Exception as e:
        return jsonify(error=str(e))

# GLOBAL FUNCTION
# def validate_token(token):
#     token = token
#     try:
#         with engine.connect() as connection:
#             qry = ("SELECT * FROM public.token WHERE public.token.user_token='dsfdsfeadawe'")
#             result = connection.execute(qry, user_token=token)
#             print(f'ini result hasil dari database {result}')
#             for item in result:
#                 print(f'ini hasil didalam result / item {item}')
#                 if item != '':
#                     return "found"
#                 else:
#                     return "not found"
#     except Exception as e:
#         return jsonify(error=str(e))
# result = validate_token('dsfdsfeadawe')
# print(f'ini hasil result{result}')

# TOP 5 MENU RECOMENDATION FOR USER LOGIN
# @app.route('/top-5-menu-for-user', methods=['GET'])
# def top_5_recommendation():
    # token = request.args.get('token')
    # status = validate_token(token)
    # if status == "not found":
    #     return jsonify({"status" : "not found"})
    # else:
    #     all = []
    #     try:
    #         with engine.connect() as connection:
    #             qry = text("SELECT public.menu.menu_id, public.menu.menu, public.menu.description, public.menu.price, count(public.bucket.menu_id) as sum \
    #                         FROM public.order JOIN public.bucket USING(order_id) JOIN public.menu USING(menu_id) \
    #                         WHERE public.order.status='complete' AND public.menu.stock > 0 \
    #                         GROUP BY public.menu.menu_id, public.menu.menu,	public.menu.description, public.menu.price \
    #                         ORDER BY sum DESC LIMIT 5")
    #             result = connection.execute(qry)
    #             for item in result:
    #                 all.append({
    #                     'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'sum':item[4]
    #                 })
    #             return(jsonify(all))
    #     except Exception as e:
    #         return jsonify(error=str(e))

# Top 5 menus who is ordered frequently and complete ======== ADMIN VIEW =========
@app.route('/top-5/menu', methods=['GET'])
def top_5_menu_ordered():
    try:
        with engine.connect() as connection:
            qry = text("SELECT public.menu.menu_id, public.menu.menu, count(public.bucket.menu_id) as sum \
                        FROM public.order JOIN public.bucket USING(order_id) \
                        JOIN public.menu USING(menu_id) WHERE public.order.status='complete' \
                        GROUP BY public.menu.menu_id, public.menu.menu \
                        ORDER BY sum DESC LIMIT 5") 
            result = connection.execute(qry)
            top_5 = []
            for item in result:
                top_5.append({
                    'menu_id':item['menu_id'], 'menu':item['menu'], 'sum':item['sum']
                })
            return jsonify(top_5)
    except Exception as e:
        return jsonify(error=str(e))

# Top 5 users who order frequently and complete ===== ADMIN VIEW ======
@app.route('/top-5/user', methods=['GET'])
def top_5_user_ordered():
    try:
        with engine.connect() as connection:
            qry = text("SELECT  public.user.user_id, public.user.name, count(public.order.user_id) as total_transaction \
                        FROM public.order JOIN public.user USING (user_id) \
                        WHERE status='complete' GROUP BY public.user.name,  public.user.user_id \
                        ORDER BY total_transaction DESC LIMIT 5")
            result = connection.execute(qry)
            top_5 = []
            for item in result:
                top_5.append({
                    'user_id':item['user_id'], 'name':item['name'], 'total_transaction':item['total_transaction']
                })
            return jsonify(top_5)
    except Exception as e:
        return jsonify(error=str(e))

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=5010)